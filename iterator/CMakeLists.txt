cmake_minimum_required(VERSION 2.8)
project(iterator)

include(../common.cmake)

add_executable(test_iterator test.cpp)
